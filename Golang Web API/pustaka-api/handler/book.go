package handler

import(
	"net/http"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"pustaka-api/book"
	"strconv"
	// "params"
)

type bookHandler struct {
	bookService book.Service
}

func NewBookHandler(bookService book.Service) *bookHandler{
	return &bookHandler{bookService}
}

func (h *bookHandler) Getbooks(c *gin.Context){
	books, err := h.bookService.FindAll()
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	var booksResponse []book.BookResponse

	for _, b := range books{
		bookResponse := convertToBookResponse(b)
		booksResponse = append(booksResponse, bookResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": booksResponse,
	})
}

func (h *bookHandler) Getbook(c *gin.Context){
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	b, err := h.bookService.FindByID(int(id))

	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}
	bookResponse := convertToBookResponse(b)
	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})

}

func (h *bookHandler) Createbook(c *gin.Context){
	//menerima title dan price
	var bookRequest book.BookRequest

	err := c.ShouldBindJSON(&bookRequest)
	if err != nil{
		errorMassages := []string{}
		for _, e := range err.(validator.ValidationErrors){
			errorMassage := fmt.Sprintf("Error on field %v, condition: %v", e.Field(), e.ActualTag())
			errorMassages = append(errorMassages, errorMassage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMassages,
		})
		return
	}

	book , err := h.bookService.Create(bookRequest)
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error":err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}

func (h *bookHandler) Updatebook(c *gin.Context){
	//menerima title dan price
	var bookRequest book.BookRequest

	err := c.ShouldBindJSON(&bookRequest)
	if err != nil{
		errorMassages := []string{}
		for _, e := range err.(validator.ValidationErrors){
			errorMassage := fmt.Sprintf("Error on field %v, condition: %v", e.Field(), e.ActualTag())
			errorMassages = append(errorMassages, errorMassage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMassages,
		})
		return
	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	book , err := h.bookService.Update(id, bookRequest)
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error":err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}

func (h *bookHandler) Deletebook(c *gin.Context){
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	b, err := h.bookService.Delete(int(id))

	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}
	bookResponse := convertToBookResponse(b)
	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})

}

func convertToBookResponse(b book.Book) book.BookResponse{
	return book.BookResponse{
		ID: b.ID,
		Title: b.Title,
		Price: b.Price,
		Description: b.Description,
		Rating: b.Rating,
		Discount:b.Discount,
	}
}