package main

import (
	// "net/http"
	"log"
	// "fmt"
	"github.com/gin-gonic/gin"
	// "github.com/go-playground/validator/v10"
	// "encoding/json"
	"pustaka-api/handler"
	"pustaka-api/book"
	"gorm.io/driver/mysql"
  "gorm.io/gorm"
)

  //ALUR
  //main
  //handler
  //service
  //repository
  //db
  //mysql
func main(){
    dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
    db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

  	if err != nil{
  		log.Fatal("DB connection error")
  	}

  	db.AutoMigrate(&book.Book{})

  	bookRepository := book.NewRepository(db)
  	bookService  := book.NewService(bookRepository)
    bookHandler := handler.NewBookHandler(bookService)
  	

  	//CRUD

  	//CREATE
  	// book := book.Book{}
  	// book.Title = "my other book"
  	// book.Price = 77777
  	// book.Discount = 30
  	// book.Rating = 5
  	// book.Description = "buku yang saya miliki lainnya"

  	// err = db.Create(&book).Error
  	// if err != nil{
  	// 	fmt.Println("Error creating book record")
  	// }

  	//READ
  	// var book book.Book

  	// err = db.First(&book).Error
  	// if err != nil{
  	// 	fmt.Println("Error creating book record")
  	// }
  	// fmt.Println("Title: ", book.Title)
  	// fmt.Println("book object %v ", book)

  	//UPDATE
  	// var book book.Book

  	// err = db.Debug().Where("id = ?", 1).First(&book).Error
  	// if err != nil{
  	// 	fmt.Println("Error creating book record")
  	// }
  	// book.Title = "Book: Update Title"
  	// err = db.Save(&book).Error
  	// if err != nil{
  	// 	fmt.Println("Error updating book record")
  	// }

  	//DELETE
  	// var book book.Book

  	// err = db.Debug().Where("id = ?", 1).First(&book).Error
  	// if err != nil{
  	// 	fmt.Println("Error finding book record")
  	// }

  	// err = db.Delete(&book).Error
  	// if err != nil{
  	// 	fmt.Println("Error delete book record")
  	// }


	router := gin.Default()

	v1 := router.Group("/v1")

	
	v1.GET("/books", bookHandler.Getbooks)
  v1.GET("/books/:id", bookHandler.Getbook)
  v1.POST("/books", bookHandler.Createbook)
  v1.PUT("/books/:id", bookHandler.Updatebook)
  v1.DELETE("/books/:id", bookHandler.Deletebook)

	router.Run()
}


